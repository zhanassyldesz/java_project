var patients = [];
var doctores = [];
var listData = [];
var page = 0

function findProduct (productId) {
    return patients[findProductKey(productId)];
}

function findProductKey (productId) {
    for (var key = 0; key < patients.length; key++) {
        if (patients[key].id == productId) {
            return key;
        }
    }
}

var patientService = {
    findAll(fn,param){
        axios
            .get('/api',{params:param})
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    findAllPages(fn){
        axios
            .get('/api/help')
            .then(response => fn(response))
    .catch(error => console.log(error))
    },
    create(patient, fn) {
        axios
            .post('/api', patient)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    findAllDoctores(fn){
        axios
            .get('/api/doctor')
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    update(id, patient, fn) {
        axios
            .put('/api/' + id, patient)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    delete(id, fn) {
        axios
            .delete('/api/' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    }
}

var Liste = Vue.extend({
    template: '#patient-list',
    data: function(){
        return {
            patients: [], searchKey: '', currentSort: 'name' , currentSortDir: 'asc', param: {} , page: 0 , listData: []
        };
    },
    computed: {
        filteredPatients() {
            return this.patients.sort((a,b) => {
                let modifier = 1;
            if(this.currentSortDir === 'desc') modifier = -1;
            if(a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
            if(a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
            return 0; }).filter((patient) => {
                return patient.name.indexOf(this.searchKey) > -1
                    || patient.doctores.name.indexOf(this.searchKey) > -1
            })
        },
        num_pages: function()
        {
            let l = this.listData.length;
            s = 3;
            return Math.ceil(l/s);
        }
    },
    methods: {
        sort: function (s) {
            if (s === this.currentSort) {
                this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
            }
            this.currentSort = s;
        },
        changeParams:function(page) {
            param.page = page
            patientService.findAll(r => {this.patients = r.data; patients = r.data},param)
        }
    },
    mounted() {
        param = {
            page: page
        };
        patientService.findAll(r => {this.patients = r.data; patients = r.data},param)
        patientService.findAllPages(r => {this.listData = r.data; listData = r.data})
    }
})


var AddPatient = Vue.extend({
    template: '#add-patient',
    data() {
        return {
            patient: {name: '', surname: '', email: '',address: '',disease: '', doctores:[]}, doctores : []
        }
    },
    methods: {
        createPatient() {
            patientService.create(this.patient, r => router.push('/'))
        }
    },
    mounted() {
        patientService.findAllDoctores(r => {this.doctores = r.data; doctores = r.data})
    }
});

var Patient = Vue.extend({
    template: '#patient',
    data: function () {
        return {patient: findProduct(this.$route.params.patient_id)};
    }
});

var PatientEdit = Vue.extend({
    template: '#patient-edit',
    data: function () {
        return {patient: findProduct(this.$route.params.patient_id), doctores : []};
    },
    methods: {
        updatePatient: function () {
            patientService.update(this.patient.id, this.patient, r => router.push('/'))
        }
    },
    mounted() {
        patientService.findAllDoctores(r => {this.doctores = r.data; doctores = r.data})
    }
});

var PatientDelete = Vue.extend({
    template: '#patient-delete',
    data: function () {
        return {patient: findProduct(this.$route.params.patient_id)};
    },
    methods: {
        deletePatient: function () {
            patientService.delete(this.patient.id, r => router.push('/'))
        }
    }
});

var router = new VueRouter({
    routes: [
        {path: '/', component: Liste},
        {path: '/patient/:patient_id', component: Patient, name: 'patient'},
        {path: '/add-patient', component: AddPatient},
        {path: '/patient/:patient_id/edit', component: PatientEdit, name: 'patient-edit'},
        {path: '/patient/:patient_id/delete', component: PatientDelete, name: 'patient-delete'}
    ]
})

new Vue({
    router
}).$mount('#app')

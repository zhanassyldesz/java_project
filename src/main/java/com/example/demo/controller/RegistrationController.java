package com.example.demo.controller;

import com.example.demo.model.Doctor;
import com.example.demo.model.Registration;
import com.example.demo.repository.DoctorRepository;
import com.example.demo.repository.PatientRepository;
import com.example.demo.services.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
@RequiredArgsConstructor
public class RegistrationController {

    @Autowired
    PatientRepository userRepo;

    @Autowired
    DoctorRepository doctorRepo;

    @Autowired
    RegistrationService service;

    @Autowired
    public RegistrationController(PatientRepository userRepo, DoctorRepository doctorRepo, RegistrationService service)
    {
        this.userRepo = userRepo;
        this.doctorRepo = doctorRepo;
        this.service = service;
    }

    @GetMapping(params = {"page"})
    public List<Registration> findAll(@RequestParam("page") int page)
    {
        Page<Registration> list = service.findAll(page);
        List<Registration> patientsEntity = list.getContent();
        return patientsEntity;
//        return ResponseEntity.ok(service.findAll(page));
    }

    @GetMapping("/help")
    public ResponseEntity<List<Registration>> findAllPages()
    {
        return ResponseEntity.ok(userRepo.findAll());
    }

    @GetMapping("/doctor")
    public ResponseEntity<List<Doctor>> findAllDoctores()
    {
        return ResponseEntity.ok(doctorRepo.findAll());
    }

    @PostMapping(consumes = "application/json")
    public Registration create(@Valid @RequestBody Registration patient) {
        return service.save(patient);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<Registration> update(@PathVariable Integer id, @Valid @RequestBody Registration patient) {
        if (!service.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(service.save(patient));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        if (!service.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        service.deleteById(id);
        return ResponseEntity.ok().build();
    }

    //    @Autowired
//    DoctorRepository doctorRepo;
//
//
//    @RequestMapping("/finally")
//    public String home(Model model, @RequestParam(defaultValue = "0") int page, @RequestParam Optional<String> sortBy )
//    {
//        model.addAttribute("users", userRepo.findAll(new PageRequest(page,2, Sort.Direction.ASC,sortBy.orElse("id"))));
//        model.addAttribute("currentPage",page);
//        return "test";
//    }
//
//
//    @GetMapping
//    public String finalPage(Model model)
//    {
//        model.addAttribute("doctor", new Doctor());
//        List <Doctor> doctors = doctorRepo.findAll();
//        model.addAttribute("doctors",doctors);
//        model.addAttribute("patient", new Registration());
//        return "final";
//    }
//
//    @RequestMapping(value = "/redirection", method = RequestMethod.GET)
//    public String homePage() {
//        return "home";
//    }
//
//
//    @PostMapping
//    public String savePatient(@ModelAttribute("patient") @Valid Registration patient, Errors errors) {
//
//        if (errors.hasErrors()) {
//            return "final";
//        }
//        userRepo.save(patient);
//        return "redirect:/final/finally";
//    }
//
//    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
//    public String update(@PathVariable int id, Model model)
//    {
//        Registration patient = userRepo.findById(id);
//        model.addAttribute("patient",patient);
//        List <Doctor> doctors = doctorRepo.findAll();
//        model.addAttribute("doctors",doctors);
//        return "update";
//    }
//
//    @RequestMapping(value = "/update/{id}",method = RequestMethod.POST)
//    public String updateSave(@ModelAttribute("patient") @Valid Registration patient, @PathVariable String id)
//    {
//        int patientId = Integer.parseInt(id);
//        System.out.println(patientId);
//        Registration p = userRepo.findById(patientId);
//        System.out.println(p);
//
//        p.setName(patient.getName());
//        p.setSurname(patient.getSurname());
//        p.setEmail(patient.getEmail());
//        p.setAddress(patient.getAddress());
//        p.setDisease(patient.getDisease());
//        p.setDoctores(patient.getDoctores());
//        userRepo.save(p);
//
//        return "redirect:/final/finally";
//
//    }
//
//    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
//    public String delete(@PathVariable int id)
//    {
//        Registration fooOptional = userRepo.findById(id);
//        userRepo.delete(fooOptional);
//        return "redirect:/final/finally";
//    }
}

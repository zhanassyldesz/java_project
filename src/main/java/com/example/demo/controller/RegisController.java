package com.example.demo.controller;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Collections;

@Controller
public class RegisController {
    @Autowired
    private UserRepository userRepo;

    @GetMapping("/regis")
    public String registration(Model model)
    {
        return "regis";
    }

    @PostMapping("/regis")
    public String addUser(@Valid User user, Model model)
    {
//        if(bindingResult.hasErrors()){
//            return "regis";
//        }

        if( user.getPassword() != null && !user.getPassword().equals(user.getPassword2())){
            model.addAttribute("passwordError","Passwords are different!");
        }

        User u = userRepo.findByUsername(user.getUsername());

        if( u != null) {
            model.addAttribute("message","User exists!");
            return "regis";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userRepo.save(user);

        return "redirect:/login";
    }
}

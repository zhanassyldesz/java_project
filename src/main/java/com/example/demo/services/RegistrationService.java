package com.example.demo.services;

import com.example.demo.model.Registration;
import com.example.demo.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class RegistrationService {

    private final PatientRepository patientRespository;

    public Page<Registration> findAll(int page) {
        Pageable pages = PageRequest.of(page,3);
        Page<Registration> patients = patientRespository.findAll(pages);
//        List<Registration> patientsEntity = patients.getContent();

        return patients;
    }

    public Optional<Registration> findById(Integer id) {
        return patientRespository.findById(id);
    }

    public Registration save(Registration stock) {
        return patientRespository.save(stock);
    }

    public void deleteById(Integer id) {
        patientRespository.deleteById(id);
    }
}
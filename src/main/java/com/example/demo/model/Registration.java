package com.example.demo.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.persistence.Entity;


@Entity
@Table(name = "Patient")
public class Registration {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message="Name is required")
    private String name;

    @NotBlank(message="Surname is required")
    private String surname;

    @NotBlank(message="Email is required")
    @Pattern(regexp="^(.+)@(.+)$",
            message="Must be formatted [a-z]@[a-z].[a-z]")
    private String email;

    @NotBlank(message="Address is required")
    private String address;

    @NotBlank(message="Disease is required")
    private String disease;

    public Registration() { }

    public Registration(String name, String surname, String email, String address , String disease , Doctor doctores)
    {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.disease = disease;
        this.doctores = doctores;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() { return surname;}

    public String getEmail() {
        return email;
    }

    public String getAddress()
    {
        return address;
    }

    public String getDisease()
    {
        return disease;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    @ManyToOne
    @JoinColumn(name = "doctor_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Doctor doctores;

    public void setDoctores(Doctor doctores) {
        this.doctores = doctores;
    }

    public Doctor getDoctores() {
        return doctores;
    }
}